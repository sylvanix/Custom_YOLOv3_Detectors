<!--  -->
## Making your own YOLOv3 detector


[//]: # (Image References)
[image1]: ./images/detections.png


![alt text][image1]  


Why is YOLOv3 better than YOLOv2? Well, there are several features that make it more than just an "incremental improvement". This article https://mc.ai/yolo3-a-huge-improvement/, explains it well. For me, the most important points are that v3 performs better for small object detection, and still does it incredibly fast.  


---  

##### Build opencv wherever you want it    
I do not rely on the older version of OpenCV that can be installed as a binary with apt-get. I build OpenCV in the "site-packages" directory of the Python version that I use to build it. I installed Python in /opt but then I made a virtual installation in my home directory. It is a hidden directory named ".ml36". If you are using Ubuntu Linux, see this guide for more information on building Python & OpenCV: https://github.com/drforester/Setting-up-your-Python-ML-system  
The full path of the OpenCV directory on my system is: `/home/telemaque/.ml36/lib/python3.6/site-packages/opencv`  
Make a note of the path to your OpenCV directory.  

---
##### Modify the Darknet Makefile  

Clone the Darknet repository:  
```
~$ git clone https://github.com/pjreddie/darknet
~$ cd darknet
```

Assuming that you have CUDA, cudnn, and OpenCV installed, make the first lines of your Makefile look like this:  
```
GPU=1
CUDNN=1
OPENCV=1
OPENMP=0
DEBUG=0

ARCH= -gencode arch=compute_61,code=compute_61
```

The "ARCH" entry specifies that my GPU is of the Pascal type (that's what the 61 means).    

To specify the path of our OpenCV libraries, we must make the following changes:  

After the line "ifeq ($(OPENCV), 1), comment out the lines:  

LDFLAGS+= `pkg-config --libs opencv`   
COMMON+= `pkg-config --cflags opencv`  


and add these lines just below them:  
```
OPENCV_LIBPATH=/home/telemaque/.ml36/lib/python3.6/site-packages/opencv/lib  
OPENCV_INCLUDEPATH=/home/telemaque/.ml36/lib/python3.6/site-packages/opencv/include  
OPENCV_LIBS=-lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_imgcodecs -lopencv_photo \  
            -lopencv_video -lopencv_videoio -lopencv_videostab  
LDFLAGS+= -L $(OPENCV_LIBPATH) $(OPENCV_LIBS)  
COMMON+= -I $(OPENCV_INCLUDEPATH)  
```

---
##### Writing out detection results using OpenCV  

To write the OpenCV video out:  
Per (https://github.com/pjreddie/darknet/issues/102),    
add the following lines in src/image.c after the line  
cvShowImage(buff, disp);

```
    cvShowImage(buff, disp);
	{
		CvSize size;
		{
			size.width = disp->width, size.height = disp->height;
		}

		static CvVideoWriter* output_video = NULL;    // cv::VideoWriter output_video;
		if (output_video == NULL)
		{
			printf("\n SRC output_video = %p \n", output_video);
			const char* output_name = "test_out.avi";
			output_video = cvCreateVideoWriter(output_name, CV_FOURCC('D', 'I', 'V', 'X'), 25, size, 1);
			printf("\n cvCreateVideoWriter, DST output_video = %p  \n", output_video);
		}

		cvWriteFrame(output_video, disp);
		printf("\n cvWriteFrame \n");
	}
```    
Now build using the Makefile.  
Should you decide that you don't like OpenCV writing out video files every time you run the detector, simply comment out the lines below "cvShowImage(buff, disp);" and run make again.  

---
##### Change detection box width:
I like to change the detection box and text in the following ways:  

Change the multiplier in this line in image.c:  
`int width = im.h * .006;`

Change text size multiplier in this line:  
`image label = get_label(alphabet, labelstr, (im.h*.03));`  

Makethe text box "float" above the detection box in the next line by adjusting the "width" parameter like so:    
`draw_label(im, top+(width-im.h*0.01), left, label, rgb);`  

---

##### Install ffmpeg  
Because I collect all training and testing imagery as video recorded from a DSLR camera or a mobile phone camera, I need to extract the frames as sequentially numbered images. I do this with ffmpeg. First though, make a directory with the same name as the video file. Then move the video file inside this directory. Create four subdirectories inside:  
```
~$ mkdir extract
~$ mkdir resize
~$ mkdir labels
~$ mkdir pairs
```

```
~$ sudo apt-get install ffmpeg
~$ ffmpeg -i my_video.mp4 -r 30 -f image2 %04d.png  
```  

Make sure that you know the correct framerate for your video when using the -r option.  
Next, move all extracted images to the dir named "extract":  
`~$ mv *.png extract`  

---

##### Install the BIMP plugin for GIMP  
To resize my extracted images, assuming that I want to do that, I use the bimp plugin for Gimp.  
```
~$ sudo apt-get install gimp
~$ sudo apt-get install libgimp2.0-dev libpcre3-dev
~$ git clone https://github.com/alessandrofrancesconi/gimp-plugin-bimp
~$ cd gimp-plugin-bimp
~$ make && make install
```

Now when you run Gimp, there is the option to do batch processing of images.
From the "File" drop-down menu, select "Batch Image Manipulation...". Save the resized images to the directory "resize". For a directory containing thousands for large images, this can take several minutes to complete. Have a beer and relax.   

---  
##### Install labelImg  
When you have extracted and resized the images from the videos, you can step through them and draw labeled bounding boxes for each object you want to detect. With labelImg you can choose to save your labels in either VOC or Yolo format. I am used to saving in VOC format then converting to Yolo format for training, so I will continue to do it that way.  
```
~$ sudo apt-get install pyqt5-dev-tools
~$ sudo apt install python3-pip
~$ sudo pip3 install lxml
~$ git clone https://github.com/tzutalin/labelImg
~$ cd labelImg
~$ make qt5py3
~$ python3 labelImg.py
```  
Save the labels to the directory "labels".

---
##### Convert VOC labels to Yolo format  
```
import os
from os import walk
from PIL import Image

classes = {"some_class_name":0, "another_class_name":1}

def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)


""" Configure Paths"""
vid_dir = 'path_to_images'
mypath  = vid_dir + '/labels' # subdir for label files output from labelImg
outpath = vid_dir + '/pairs'  # subdir in which to write the yolo formatted labels

""" Get input text file list """
txt_name_list = []
for (dirpath, dirnames, filenames) in walk(mypath):
    txt_name_list.extend(filenames)
    break
print(txt_name_list)

""" Process """
for txt_name in txt_name_list:

    """ Open input text files """
    txt_path = mypath + '/' + txt_name
    print("Input:" + txt_path)
    txt_file = open(txt_path, "r")
    lines = txt_file.read().split('\r\n')

    """ Open output text files """
    nomen = txt_name.split('.')[0]+'.txt'
    txt_outpath = outpath + '/' + nomen
    print("Output:" + txt_outpath)
    txt_outfile = open(txt_outpath, "w")


    """ Convert the data to YOLO format """
    ct = 0
    #print(lines)
    for line in lines:
        if(len(line) >= 2):
            ct = ct + 1
            linelist = line.replace('\t','').split('\n')
            #print(linelist)
            for xline in linelist:
                if xline.startswith('<filename>'):
                    img_name = xline.split('<filename>')[1].split('</filename>')[0]
                    print('image name:', img_name)
                if xline.startswith('<name>'):
                    cname = xline.split('<name>')[1].split('</name>')[0]
                    cls_id = classes[cname]
                    print(cname, cls_id)
                if xline.startswith('<width>'):
                    w = int(xline.split('<width>')[1].split('</width>')[0])
                if xline.startswith('<height>'):
                    h = int(xline.split('<height>')[1].split('</height>')[0])
                if xline.startswith('<xmin>'):
                    xmin = xline.split('<xmin>')[1].split('</xmin>')[0]
                    print('xmin:', xmin)
                if xline.startswith('<xmax>'):
                    xmax = xline.split('<xmax>')[1].split('</xmax>')[0]
                    print('xmax:', xmax)
                if xline.startswith('<ymin>'):
                    ymin = xline.split('<ymin>')[1].split('</ymin>')[0]
                    print('ymin:', ymin)
                if xline.startswith('<ymax>'):
                    ymax = xline.split('<ymax>')[1].split('</ymax>')[0]
                    print('ymax:', ymax)
                if xline.startswith('</object>'):
                    b = (float(xmin), float(xmax), float(ymin), float(ymax))
                    bb = convert((w,h), b)
                    print('YOLO Bounding Boxes:', bb)
                    txt_outfile.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')
            print('')
```

---
##### Copy appropriate images to yolo-formatted labels directory   
In the previous section, we saved the yolo-formatted labels to a directory named "pairs". I chose that name because it reminds me that I need to copy each image to this directory to be paired with its label. The following script will find all images in the "resize" directory having a label in "pairs", and place a copy there.  

```
import os
from shutil import copyfile

# returns a file path generator for all files under data_dir
def get_imgs(data_dir):
    fileiter = (os.path.join(root, f)
                for root, _, files in os.walk(data_dir)
                for f in files)
    return fileiter


vid_dir      = 'path_to_images'
img_dir      = 'resize'
labels_dir   = 'labels'
pairs_dir    = 'pairs'
first_img_nb = 0

fileiter = get_imgs(vid_dir + '/' + img_dir)
pngs = [f for f in fileiter if os.path.splitext(f)[1] == '.png']
#print(pngs)
#print('')

fileiter = get_imgs(vid_dir + '/' + labels_dir)
xmls = [f for f in fileiter if os.path.splitext(f)[1] == '.xml']
#print(xmls)
#print('')

xml_numbs = sorted([x.split('/')[-1].split('.')[0] for x in xmls])
#print(len(xml_numbs))
#print('')

png_matches = [x for x in pngs
               if x.split('/')[-1].split('.')[0] in xml_numbs]
print(png_matches)

for x in png_matches:
    copyfile(x, vid_dir + '/' + pairs_dir + '/' + x.split('/')[-1])
```

---
##### One dir to rule them all  
One last script is needed before training. The script below will search in each of the videos' "pairs" directories and place renumbered copies of them into a single directory to be used for training the detector. You must enter the names of the video directories you want included in the list "vid_dirs".  
```
import os
from shutil import copyfile
import random


'''
returns a file path generator for all files under data_dir
'''
def get_imgs(data_dir):
    fileiter = (os.path.join(root, f)
                for root, _, files in os.walk(data_dir)
                for f in files)
    return fileiter



vid_dirs = ['DJI_0009','DJI_0010','DJI_0012','DJI_0015','DJI_0016',
            'DJI_0017','DJI_0021','DJI_0022','DSC_0001','DSC_0030',
            'S7_085519', S7_090223, S7_090843]
bind_dir = 'training_images'

# get list paths to all images and corresponding labels
all_imgs = []
all_labels = []
for vid_dir in vid_dirs:
    fileiter = get_imgs(vid_dir + '/pairs')
    pngs = [f for f in fileiter if os.path.splitext(f)[1] == '.png']
    for xpng in pngs:
        print(xpng)
        all_imgs.append(xpng)
        xlabel = xpng.split('.png')[0] + '.txt'
        print(xlabel)
        all_labels.append(xlabel)


# randomize the list image and label list indexes
inds = [x for x in range(len(all_imgs))]
random.shuffle(inds)


# copy newly ordered and numbered image/label pairs to the dataset directory
count = 0
for iimg in range(len(all_imgs)):
    ii = inds[count]
    ixnum = str(ii).zfill(4)
    print(ixnum)
    xpng = all_imgs[ii]
    print(xpng)
    xlabel = all_labels[ii]
    print(xlabel)
    xpng_new = bind_dir+'/'+ixnum +'.png'
    print(xpng_new)
    xlabel_new = bind_dir+'/'+ixnum +'.txt'
    print(xlabel_new)
    # copy the image to dataset dir
    copyfile(xpng, xpng_new)
    # copy the corresponding label to dataset dir
    copyfile(xlabel, xlabel_new)
    count += 1
    print('')
```

---
##### train.txt and test.txt  
Sucker! I lied, there is one more Python script to run. This script creates the text files train.txt and test.txt needed by Darknet. You control the ratio of training to test images with the variable "percentage_test".  

```
import glob, os

# Directory where the data will reside
path_data = '/home/telemaque/My_Detector/detector_training/'

# Percentage of images to be used for the test set
percentage_test = 20;

# Create and/or truncate train.txt and test.txt
file_train = open('train.txt', 'w')  
file_test = open('test.txt', 'w')

# Populate train.txt and test.txt
counter = 1  
index_test = round(100 / percentage_test)  
for pathAndFilename in glob.iglob(os.path.join(path_data, "*.png")):  
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))

    if counter == index_test:
        counter = 1
        file_test.write(path_data + title + '.png' + "\n")
    else:
        file_train.write(path_data + title + '.png' + "\n")
        counter = counter + 1
```

Copy train.txt and test.txt to the darknet directory.  

---
##### Increase the save frequency  
There is one change that we should make to examples/detector.c before training. When the network is training, it will save the weights to the directory "backup" every 100 iterations until 900. After 900 iterations, the default setting is to save every 10,000 iterations. We would like to save more often than that with this small dataset. To change this setting, change the following line in examples/detector.c  
`if(i%10000==0 || (i < 1000 && i%100 == 0)){`  
to  
`if(i%100==0 || (i < 1000 && i%100 == 0)){`    
or simply replace the number 10000 with whatever smaller integer you want.  

---

##### Modify the config file  

I intend to train with the pre-trained darknet53 weights. Copy the file yolov3-voc.cfg and rename it something appropriate for your detector. You need to change, in three places in the file, settings for "classes" and "filters".   

First, change the "classes" values three lines down from the three "[yolo]" lines.  

Then, calculate the value to enter for the last "filters" lines in the .cfg file:   
`filters = (nb_classes + 5) * 3`  

So, if your dataset consists of two classes, filters = 21.  

Put this value in the three "filters=" lines just a few lines above the three "[yolo]" lines.

---

##### Training:  
`~$ ./darknet detector train cfg/drones.data cfg/drones-voc.cfg darknet53.conv.74`

---
##### Test an image:  

`./darknet detector test cfg/drones.data cfg/drones-voc.cfg drones-voc_2100.weights data/drones/0510.png -out 0510`

---
##### Test a video:  

`./darknet detector demo cfg/drones.data cfg/drones-voc.cfg drones-voc_2100.weights data/test_vid.mp4 -thresh 0.4`

---
